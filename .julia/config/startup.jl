ENV["JULIA_SHELL"] = "/usr/bin/fish"
ENV["JULIA_EDITOR"] = "hx"

using Pkg: Pkg

using OhMyREPL: OhMyREPL
using Revise: Revise, includet

if isfile("Project.toml")
    Pkg.activate(".")
end
