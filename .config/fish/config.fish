if status is-interactive
    # Disable greeting
    set -g fish_greeting

    # Interactive environment variables
    set -gx EDITOR ~/.cargo/bin/hx

    # Setup
    ## Zoxide
    zoxide init fish | source
    ## Starship
    starship init fish | source
    ## fnm
    fnm env --shell fish | source

    # Abbreviations
    ## Replacements
    abbr -ag rm trash

    ## Safety options
    abbr -ag cp "cp -i"
    abbr -ag mv "mv -i"
    abbr -ag rmr "rm -r"

    ## Default options
    set -g rsync "rsync -avhL --partial --info=progress2 --no-i-r --zc=zstd --zl=9"
    abbr -ag rsync "$rsync"

    ## Podman
    abbr -ag sage "podman run -it --rm --pull newer --name sage docker.io/sagemath/sagemath:latest"

    abbr -ag typst "podman run -it --rm --pull newer --name typst -v (pwd):/volumes/src:Z ghcr.io/typst/typst:latest typst w /volumes/src/src.typ /volumes/src/output.pdf"

    set -l fedora_image registry.fedoraproject.org/fedora:(rpm -E %fedora)
    abbr -ag fedora "podman run -it --rm --pull newer --name fedora $fedora_image"

    abbr -ag pps "podman ps --format '{{.Names}}'"

    # Aliases
    alias cd z
    alias ls lsd
    alias ll "ls -l"
    alias cat bat
    alias gu gitui
    alias julia "JULIA_NUM_THREADS=16 ~/.juliaup/bin/julia"
    alias tb "toolbox run fish"
    alias todo "hx ~/todo.md"

    # Private config
    set -l private_config ~/.config/fish/private_config.fish
    if test -f $private_config
        source $private_config
    end

    function divan
        cd ~/code/tmp
        rm -r benchmark
        mkdir benchmark
        cd benchmark

        echo "target/
.ignore
Cargo.lock" >.ignore

        echo '[package]
name = "benchmark"
version = "0.0.0"
edition = "2021"
publish = false

[profile.release]
panic = "abort"

[dev-dependencies]
divan = "0.1"

[[bench]]
name = "divan"
harness = false' >Cargo.toml

        mkdir src
        touch src/lib.rs

        mkdir benches
        echo 'use divan::bench;
use std::hint::black_box as bb;

fn main() {
    divan::main();
}

#[bench]
fn bench() {
    todo!()
}' >benches/divan.rs

        echo 'target
Cargo.lock' >.ignore

        hx benches/divan.rs
    end
end
